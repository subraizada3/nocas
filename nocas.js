/*
MIT License

Copyright (c) 2017 Subramaniyam Raizada

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/*
 * cookies:
 * X-Mapping-cjildjpp @ cas.iu.edu/
 * JSESSIONID @ cas.iu.edu/cas
 * CASTGC @ cas.iu.edu/cas/
 */

var cookieXMapping;
var cookieJSESSIONID;
var cookieCASTGC;

///////////////////////////////////////////////////////////////////////////////
///   XMapping   //////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
function setCookieXMapping() {
	currentTime = new Date() / 1000;

	chrome.cookies.set({
		url: "https://cas.iu.edu/",
		name: cookieXMapping.name,
		value: cookieXMapping.value,
		domain: cookieXMapping.domain,
		path: cookieXMapping.path,
		secure: cookieXMapping.secure,
		httpOnly: cookieXMapping.httpOnly,
		expirationDate: (currentTime + 2419200) // 604800 seconds in a week * 4 = 1 month
	});
}

function removeCookieXMapping() {
	chrome.cookies.remove({
		url: "https://cas.iu.edu/",
		name: "X-Mapping-cjildjpp"
	}, setCookieXMapping);
}

function storeCookieXMapping(cookie) {
	// if this is a CAS session cookie, process it
	if (cookie.session) {
		cookieXMapping = cookie;
		removeCookieXMapping();
	} // otherwise, don't
}

function getCookieXMapping() {
	chrome.cookies.get({
		url: "https://cas.iu.edu/",
		name: "X-Mapping-cjildjpp"
	}, storeCookieXMapping);
}


///////////////////////////////////////////////////////////////////////////////
///   JSESSIONID   ////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
function setCookieJSESSIONID() {
	currentTime = new Date() / 1000;

	chrome.cookies.set({
		url: "https://cas.iu.edu/cas",
		name: cookieJSESSIONID.name,
		value: cookieJSESSIONID.value,
		domain: cookieJSESSIONID.domain,
		path: cookieJSESSIONID.path,
		secure: cookieJSESSIONID.secure,
		httpOnly: cookieJSESSIONID.httpOnly,
		expirationDate: (currentTime + 2419200) // 604800 seconds in a week * 4 = 1 month
	});
}

function removeCookieJSESSIONID() {
	chrome.cookies.remove({
		url: "https://cas.iu.edu/cas",
		name: "JSESSIONID"
	}, setCookieJSESSIONID);
}

function storeCookieJSESSIONID(cookie) {
	// if this is a CAS session cookie, process it
	if (cookie.session) {
		cookieJSESSIONID = cookie;
		removeCookieJSESSIONID();
	} // otherwise, don't
}

function getCookieJSESSIONID() {
	chrome.cookies.get({
		url: "https://cas.iu.edu/cas",
		name: "JSESSIONID"
	}, storeCookieJSESSIONID);
}


///////////////////////////////////////////////////////////////////////////////
///   CASTGC   ////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
function setCookieCASTGC() {
	currentTime = new Date() / 1000;

	chrome.cookies.set({
		url: "https://cas.iu.edu/cas/",
		name: cookieCASTGC.name,
		value: cookieCASTGC.value,
		domain: cookieCASTGC.domain,
		path: cookieCASTGC.path,
		secure: cookieCASTGC.secure,
		httpOnly: cookieCASTGC.httpOnly,
		expirationDate: (currentTime + 2419200) // 604800 seconds in a week * 4 = 1 month
	});
}

function removeCookieCASTGC() {
	chrome.cookies.remove({
		url: "https://cas.iu.edu/cas/",
		name: "CASTGC"
	}, setCookieCASTGC);
}

function storeCookieCASTGC(cookie) {
	// if this is a CAS session cookie, process it
	if (cookie.session) {
		cookieCASTGC = cookie;
		removeCookieCASTGC();
	} // otherwise, don't
}

function getCookieCASTGC() {
	chrome.cookies.get({
		url: "https://cas.iu.edu/cas/",
		name: "CASTGC"
	}, storeCookieCASTGC);
}


///////////////////////////////////////////////////////////////////////////////
///   event listener   ////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
function noCasListener(changeInfo) {
	if (changeInfo.removed === false && changeInfo.cause === "explicit") {
		getCookieXMapping();
		getCookieJSESSIONID();
		getCookieCASTGC();
	}
}


///////////////////////////////////////////////////////////////////////////////
///   delete CAS cookies   ////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
function deleteCookies(tab) {
	chrome.cookies.remove({
		url: "https://cas.iu.edu/cas/",
		name: "CASTGC"
	});
	chrome.cookies.remove({
		url: "https://cas.iu.edu/cas",
		name: "JSESSIONID"
	});
	chrome.cookies.remove({
		url: "https://cas.iu.edu/",
		name: "X-Mapping-cjildjpp"
	});
}

chrome.cookies.onChanged.addListener(noCasListener);
chrome.browserAction.onClicked.addListener(deleteCookies);
